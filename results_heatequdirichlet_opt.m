%%%
%%% Results for HeatEquationDirichlet with M = 500
%%%
%%% Methods:
%%%    Peer triplets: AP4o43bdf, AP4o43dif, AP4o33pfsa, AP4o33pa, AP4o34p
%%%    Runge-Kutta: Gauss2, LobattoIIIA/B3 
%
clear all

M = 500;
N = [16,32,64,128,256,512];
tend = 1.0;
tau = tend./N;

gauss2_error_u = [1.1826e-01 6.7110e-02 3.7065e-02 1.9275e-02 8.0273e-03 9.9657e-04];
gauss2_error_u_htn = [1.1826e-01 6.7110e-02 3.7065e-02 1.9275e-02 8.0274e-03 9.9659e-04];
gauss2_error_state = [7.0720e-03 4.1267e-03 1.9342e-03 1.3639e-03 7.3474e-04 2.7081e-04];
gauss2_error_costate = [7.0699e-03 4.1246e-03 1.9321e-03 9.1151e-04 2.1337e-04 5.8037e-06];

lobatto3_error_u = [2.0068e-01 1.1489e-01 6.3785e-02 3.3276e-02 1.3887e-02 1.7261e-03];
lobatto3_error_u_htn = [2.0068e-01 1.1489e-01 6.3785e-02 3.3276e-02 1.3887e-02 1.7261e-03];
lobatto3_error_state = [7.0720e-03 4.1267e-03 1.9342e-03 1.3639e-03 7.3473e-04 2.7081e-04];
lobatto3_error_costate = [7.0699e-03 4.1246e-03 1.9321e-03 9.1150e-04 2.1337e-04 5.8037e-06];

ap4o43dif_error_u = [7.8651e-02 3.0213e-02 9.3785e-03 2.6216e-03 6.9248e-04 1.8018e-04];
ap4o43dif_error_u_htn = [7.8371e-02 2.7778e-02 8.3404e-03 2.2918e-03 6.0091e-04 1.5431e-04];
ap4o43dif_error_state = [6.8421e-04 1.6106e-04 3.2530e-05 5.9077e-06 1.0085e-06 2.4642e-07];
ap4o43dif_error_costate = [5.5696e-07 7.3750e-09 3.7157e-09 1.4668e-10 3.8912e-12 4.2739e-12];

ap4o43bdf_error_u = [7.4341e-02 2.7803e-02 8.5339e-03 2.3720e-03 6.2580e-04 1.6080e-04];
ap4o43bdf_error_u_htn = [4.6182e-02 1.1872e-02 2.9663e-03 7.3230e-04 1.8107e-04 4.5962e-05];
ap4o43bdf_error_state = [2.4423e-04 5.1677e-05 9.9188e-06 1.7783e-06 3.0075e-07 4.0479e-08];
ap4o43bdf_error_costate = [2.7667e-07 7.0938e-09 1.7534e-09 1.3428e-10 3.5966e-12 3.6115e-13];

ap4o33pa_error_u = [3.5748e-01 3.9881e-02 3.6795e-03 3.7231e-04 7.1676e-05 2.4944e-05];
ap4o33pa_error_u_htn = [3.5748e-01 3.9881e-02 3.6795e-03 3.7244e-04 7.1590e-05 1.1665e-05];
ap4o33pa_error_state = [1.6077e-04 4.2438e-05 9.0542e-06 1.3640e-06 1.5889e-07 1.1367e-08];
ap4o33pa_error_costate = [5.2617e-07 7.5224e-09 4.2353e-09 1.4067e-10 2.8534e-12 1.6283e-12];

ap4o43p_error_u = [2.9705e-02 2.6884e-03 3.5731e-04 6.3317e-05 9.9684e-06 1.4447e-05];
ap4o43p_error_u_htn = [5.4392e-02 1.2898e-02 1.9115e-03 2.6664e-04 2.7811e-05 1.3812e-05];
ap4o43p_error_state = [1.6849e-03 1.7262e-04 7.9100e-06 3.6775e-07 4.5790e-08 1.3825e-08];
ap4o43p_error_costate = [1.0973e-06 3.1468e-08 1.3478e-10 4.9947e-11 1.3692e-12 2.3561e-12];

ap4o33pfs_error_u = [5.7878e-02 1.1097e-02 1.8983e-03 2.8122e-04 3.8580e-05 5.1087e-06];
ap4o33pfs_error_u_htn = [1.3590e+00 3.1751e-01 5.9695e-02 9.7849e-03 1.5659e-03 2.6632e-04];
ap4o33pfs_error_state = [1.7477e-03 2.8768e-04 3.8785e-05 4.7727e-06 5.7311e-07 6.8864e-08];
ap4o33pfs_error_costate = [7.4877e-05 6.6924e-06 4.6606e-07 2.7223e-08 1.5505e-09 9.3082e-11];

%
figure
loglog(tau(1:6),ap4o43bdf_error_u(1:6),'y-s','LineWidth',1.5,'MarkerSize',10);
hold on;
loglog(tau(1:6),ap4o43dif_error_u(1:6),'m-*','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),gauss2_error_u(1:6),'c-d','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),lobatto3_error_u(1:6),'b-v','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pa_error_u(1:6),'g-+','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o43p_error_u(1:6),'r-^','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pfs_error_u(1:6),'Color',[0.8500 0.3250 0.0980],...
    'Marker','o','LineStyle','-','LineWidth',1.5,'MarkerSize',10);
loglog([tau(6),tau(1)],5e-1*[1/32,1],'k--*','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],5e-2*[1/32^2,1],'k--v','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],1e-2*[1/32^3,1],'k--o','LineWidth',1.5,'MarkerSize',5);
hold off;
axis([1e-3 1e-1 1e-7 1e0]);
set(gca, 'XTick', [1e-3, 1e-2, 1e-1]);
set(gca, 'YTick', [1e-6 1e-5 1e-4, 1e-3, 1e-2, 1e-1, 1e0]);
set(gca,'fontsize',15);
grid on;
ylabel('MAXIMUM CONTROL ERROR');
xlabel('TIMESTEP');
legend('AP4o43bdf','AP4o43dif','Gauss2','LobattoIIIA/B3',...
       'AP4o33pa','AP4o43p','AP4o33pfs',...
       'order 1','order 2','order 3','Location','southeast','FontSize',11);
title('Dirichlet Heat Problem: M=500');

%
figure
loglog(tau(1:6),ap4o43bdf_error_state(1:6),'y-s','LineWidth',1.5,'MarkerSize',10);
hold on;
loglog(tau(1:6),ap4o43dif_error_state(1:6),'m-*','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),gauss2_error_state(1:6),'c-d','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),lobatto3_error_state(1:6),'b-v','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pa_error_state(1:6),'g-+','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o43p_error_state(1:6),'r-^','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pfs_error_state(1:6),'Color',[0.8500 0.3250 0.0980],...
    'Marker','o','LineStyle','-','LineWidth',1.5,'MarkerSize',10);
loglog([tau(6),tau(1)],3e-2*[1/32,1],'k--*','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],4e-3*[1/32^3,1],'k--v','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],5e-4*[1/32^4,1],'k--o','LineWidth',1.5,'MarkerSize',5);
hold off;
axis([1e-3 1e-1 1e-11 1e-1]);
set(gca, 'XTick', [1e-2, 1e-1]);
set(gca, 'yTick', [1e-8 1e-6 1e-4 1e-2]);
set(gca,'fontsize',15);
grid on;
ylabel('MAXIMUM ERROR OF Y(T)');
xlabel('TIMESTEP');
legend('AP4o43bdf','AP4o43dif','Gauss2','LobattoIIIA/B3',...
       'AP4o33pa','AP4o43p','AP4o33pfs',...
       'order 1','order 3','order 4','Location','southeast','FontSize',11);
title('Dirichlet Heat Problem: M=500');

%
figure
loglog(tau(1:6),ap4o43bdf_error_costate(1:6),'y-s','LineWidth',1.5,'MarkerSize',10);
hold on;
loglog(tau(1:6),ap4o43dif_error_costate(1:6),'m-*','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),gauss2_error_costate(1:6),'c-d','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),lobatto3_error_costate(1:6),'b-v','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pa_error_costate(1:6),'g-+','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o43p_error_costate(1:6),'r-^','LineWidth',1.5,'MarkerSize',10);
loglog(tau(1:6),ap4o33pfs_error_costate(1:6),'Color',[0.8500 0.3250 0.0980],...
    'Marker','o','LineStyle','-','LineWidth',1.5,'MarkerSize',10);
loglog([tau(6),tau(1)],3e-2*[1/32,1],'k--*','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],4e-6*[1/32^3,1],'k--v','LineWidth',1.5,'MarkerSize',5);
loglog([tau(6),tau(1)],5e-9*[1/32^4,1],'k--o','LineWidth',1.5,'MarkerSize',5);
hold off;
axis([1e-3 1e-1 1e-18 1e-1]);
set(gca, 'XTick', [1e-2, 1e-1]);
set(gca, 'yTick', [1e-12 1e-10 1e-8 1e-6 1e-4 1e-2]);
set(gca,'fontsize',15);
grid on;
ylabel('MAXIMUM ERROR OF P(0)');
xlabel('TIMESTEP');
legend('AP4o43bdf','AP4o43dif','Gauss2','LobattoIIIA/B3',...
       'AP4o33pa','AP4o43p','AP4o33pfs',...
       'order 1','order 3','order 4','Location','southeast','FontSize',11);
title('Dirichlet Heat Problem: M=500');

