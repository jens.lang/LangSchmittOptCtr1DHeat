function [C, gradUC] = srk_opt(u,fh,options)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  
%%%    MIN_U C(y(T)[U])
%%%  SRK_OPT computes ojective function and its gradient with
%%%    respect to the control U.
%%%
%%%  Solver: Symplectic Runge-Kutta Methods
%%%
%%%  U = (U_01,...,U_0s,U_11,...,U_1s,...,U_N1,...,U_Ns)^T
%%%  The control is a column vector of size (N+1)*s+1, where N+1 is the
%%%  number of times steps of the overall symplectic rk method.
%%%  It approximates the control variable u(t)
%%%  at time points t_m+c_ih, m=0,1,...,N, i=1,...,s.
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funcF = fh.F;
funcJacY = fh.JacFY;
funcJacU = fh.JacFU;
funcObjF = fh.ObjF;
funcJacObjFY = fh.JacObjFY;
y0 = fh.Y0;
tspan = fh.tspan;
coeff = options.scheme;
h = options.InitialStep;
t_m = tspan(1);
t_end = tspan(end);
dimSys = length(y0);
s = coeff.s;
c = coeff.c;
matA = coeff.A;
matB = coeff.B;
vecB = coeff.b;
vecIP0 = coeff.vecIP0;
newJac = true;
noNewton = 0;
hmin = 10^-12;
matLin = sparse(s*dimSys,s*dimSys);
Ym = y0;
vecNewton = zeros(s*dimSys,1);
es = ones(1,s);
ySlope = kron(es,y0);
yAll = y0;
yStageAll = [];
pStage = zeros(dimSys,s);
pStageAll = [];
CellJac = cell(1,s);
INFONEWTON = 0;

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% start forward calculation
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%%% loop over time steps
m = 0;
while (t_m  < (t_end - hmin))
 
  Um = u(m*s+1:(m+1)*s)';  
  if newJac
     u_m = Um*vecIP0;
    JacY = funcJacY(t_m,Ym,u_m);
  end
    
  M = speye(s*dimSys) - h*sparse(kron(matA,JacY));
  [ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(M);

  %%% call Newton method
  k = 0;   
  gk = ySlope; 
  if options.LinearProblem
    gk = gk*0;
    options.NewtonTol = 1e20;
  end
      
  run_while = true;   
  
  %%% start Newton method     
  while run_while         
        k = k+1;
        ymk = kron(es,Ym) + h*gk*matA';
        for j=1:s
            i1 = (j-1)*dimSys+1;
            i2 = j*dimSys;
            t_mj = t_m + c(j)*h;  
            u_mj = u(m*s+j);
            vecNewton(i1:i2) = gk(:,j) - funcF(t_mj,ymk(:,j),u_mj);
        end
        F_gk = vecNewton;  
        delta_gk = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ -F_gk))));
        for j=1:s
            i1 = (j-1)*dimSys+1;
            i2 = j*dimSys;
            gk(:,j) = gk(:,j) + delta_gk(i1:i2);   
            vecNewton(i1:i2) = gk(:,j);    
        end
        if ~options.NewtonFixed %%% convergence test
            test = max(abs(delta_gk)./(1+abs(vecNewton)));                
            run_while = k < 1 || (test > options.NewtonTol && ...
                                  k < options.NewtonMax);
            if (k == options.NewtonMax) 
                fprintf('Newton failed at t=%12.4e\n',t_m+h);
            end
        else %%% fixed number of Newton steps
            run_while = k < options.NewtonMax;
        end            
  end %%% end Newton method

    ySlope = gk;
 yStageAll = [yStageAll,kron(es,Ym) + h*gk*matA'];
        Ym = Ym + h*ySlope*vecB;
      yAll = [yAll,Ym];
      
       t_m = t_m + h;
         m = m + 1;
  noNewton = noNewton + k;
   
end %%% end loop over time 
N = m;

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% compute objective function C(y_h(T))
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

yT = Ym;
C = funcObjF(yT);

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% end forward calculation
%   solution vector: y(T), dim = dimSys
%   overall solution vector: yAll, dim = dimSys*(N+1) 
%   storage: Ym = yAll(:,m+1), m=0,...,N+1
%   overall stage solution vector: yStageAll, dim = dimSys*s*(N+1) 
%   storage: Ymi = yStageAll(:,m*s+i), m=0,...,N, i=1,...,s
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if nargout > 1
    
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% start backward calculation
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%%% adjoint initial values
t_m = tspan(2);
Pm = funcJacObjFY(yT);
pAll = Pm;
m = m - 1;

%%% loop over time steps
while (t_m > tspan(1) + hmin)
    
    for i=1:s
           tmi = t_m + (coeff.c(i)-1)*h;
           ymi = yStageAll(:,m*s+i);
           umi = u(m*s+i);
         Jacmi = funcJacY(tmi,ymi,umi)';
    CellJac{i} = Jacmi;
        vecCol = (i-1)*dimSys+1:i*dimSys;
       matLin(:,vecCol) = h*kron(matB(:,i),Jacmi);
    end
    matLin = speye(s*dimSys) - sparse(matLin);
    
    [ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(matLin);
    vecLin = kron(es',Pm);
    vecP = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ vecLin)))); 
    for i=1:s
        pStage(:,i) = vecP((i-1)*dimSys+1:i*dimSys);
        Pm = Pm + h*vecB(i)*CellJac{i}*pStage(:,i);
    end

         pAll = [Pm,pAll];
    pStageAll = [pStage,pStageAll];
          t_m = t_m - h;
            m = m - 1;
     noNewton = noNewton + 1;

end %%% end loop over time steps

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% end backward calculation
%   solution vector: p(0), dim = dimSys
%   overall solution vector: pAll, dim = dimSys*(N+1) 
%   storage: Pm = pAll(:,m+1), m=0,...,N+1
%   overall stage solution vector: pStageAll, dim = dimSys*s*(N+1) 
%   storage: Pmi = pStageAll(:,m*s+i), m=0,...,N, i=1,...,s
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if INFONEWTON==1
   fprintf('Number of Newton Steps = %d for %d time steps.\n',...
       noNewton,2*N);
end

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% compute gradient
%   grad_U C(y_h(T))
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

gradUC = zeros(length(u),1);
t_0 = tspan(1);

for m=1:N
    for i=1:s
      tmi = t_0 + (m-1)*h + c(i)*h;
      ymi = yStageAll(:,(m-1)*s+i);
      pmi = pStageAll(:,(m-1)*s+i);
      umi = u((m-1)*s+i);
   JacUmi = funcJacU(tmi,ymi,umi)';   
   gradUC((m-1)*s+i) = h*vecB(i)*JacUmi*pmi;
    end
end

end %%% end function
end %%% nargout > 1


