function Coeffs = def_symplectic_rk_meth(meth)
%%%
%%% Coefficients for selected symplectic Runge-Kutta methods
%%%
%%% Gauss2, Gauss3, LobattoIIIA2, LobattoIIIA3
%%% [1] E. Hairer, C. Lubich, G. Wanner
%%%     Geometric Numerical Integration, Structure-Preserving Algorithms
%%%       for Ordinary Differential Equations
%%%     Springer Series in Computational Mathematics, vol. 31. Springer,
%%%     Heidelberg, Berlin, 2006
%%%
if strcmpi(meth,'gauss2') 
    name = 'GAUSS2';    
    s = 2; 
    q = sqrt(3);
    c = [(3-q)/6; (3+q)/6];
    b = [1/2; 1/2];
    A = [1/4, (3-2*q)/12;
         (3+2*q)/12, 1/4];
    B = diag(1./b)*A'*diag(b);
    matV2 = [c.^0,c.^1];
    vecIP0 = (matV2')^(-1)*[1;0];  
    vecIP1 = (matV2')^(-1)*[1;1];    
  
elseif strcmpi(meth,'gauss3') 
    name = 'GAUSS3'; 
    s = 3; 
    q = sqrt(15);
    c = [(5-q)/10; 1/2; (5+q)/10];
    b = [5/18; 4/9; 5/18];
    A = [5/36, (10-3*q)/45, (25-6*q)/180;
         (10+3*q)/72, 2/9, (10-3*q)/72;
         (25+6*q)/180, (10+3*q)/45, 5/36];
    B = diag(1./b)*A'*diag(b);
    matV3 = [c.^0,c.^1,c.^2];
    vecIP0 = (matV3')^(-1)*[1;0;0];  
    vecIP1 = (matV3')^(-1)*[1;1;1];  
    
elseif strcmpi(meth,'lobattoIIIA2') 
    name = 'LOBATTOIIIA2';     
    s = 2; 
    c = [0; 1];
    b = [1/2; 1/2];
    A = [0, 0;
         1/2, 1/2;];
    B = diag(1./b)*A'*diag(b);
    matV2 = [c.^0,c.^1];
    vecIP0 = (matV2')^(-1)*[1;0];  
    vecIP1 = (matV2')^(-1)*[1;1];    
    
elseif strcmpi(meth,'lobattoIIIA3') 
    name = 'LOBATTOIIIA3';    
    s = 3; 
    c = [0; 1/2; 1];
    b = [1/6; 2/3; 1/6];
    A = [0, 0, 0;
         5/24, 1/3, -1/24;
         1/6, 2/3, 1/6];
    B = diag(1./b)*A'*diag(b);
    matV3 = [c.^0,c.^1,c.^2];
    vecIP0 = (matV3')^(-1)*[1;0;0];  
    vecIP1 = (matV3')^(-1)*[1;1;1];    
    
end

Coeffs = struct('s',s,...
                'c',c,...
                'b',b,...
                'A',A,...
                'B',B,...
                'vecIP0',vecIP0,...
                'vecIP1',vecIP1,...
                'name',name);    
            
end
            
