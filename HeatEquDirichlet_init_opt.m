function fh = HeatEquDirichlet_init_opt(varargin)

%%% spatial discretization: cell centred
n = 500;  
xi = 1/n;
e = ones(n,1);
M = (1/xi^2)*spdiags([e -2*e e],-1:1,n,n);
M(1,1) = -1/xi^2;
M(n,n) = -3/xi^2;

%%% eigenvectors and eigenvalues of M
EVec = zeros(n,n);
EVal = zeros(1,n);
scal = zeros(1,n);
for k=1:n
    om = (k-1/2)*pi/(2*n);
    for j=1:n
        EVec(j,k) = cos(om*(2*j-1));
    end
    scal(k) = 1/sqrt(EVec(:,k)'*EVec(:,k));
    EVec(:,k) = scal(k)*EVec(:,k);
    EVal(k) = -(4/xi^2)*sin(om)^2;
end

%%% initial conditions
y0 = [ones(n,1); 0];
tspan = [0 1];
aut = false;

%%% reference solution
ETA = zeros(2,n);
pp1 = 1/75; 
pp2 = 1/75; 
pc1 = EVec(n,1)*pp1;
pc2 = EVec(n,2)*pp2;
pT = pp1*EVec(:,1)+pp2*EVec(:,2);   % p at t=T
T = tspan(2);
ETA(1,:) = y0(1:n)'*EVec;
gamma = 2/xi^2;
for k=1:n
    fak1 = (exp((EVal(k)+EVal(1))*T)-1)/(EVal(k)+EVal(1))*pc1;
    fak2 = (exp((EVal(k)+EVal(2))*T)-1)/(EVal(k)+EVal(2))*pc2;
    fakg = EVec(n,k)*(fak1+fak2)*gamma^2;
    ETA(2,k) = ETA(1,k)*exp(EVal(k)*T)-fakg;
end
y_ref = EVec*ETA(2,:)';                         % y at t=T
y_target = y_ref-pT;
p_ref = pp1*exp(EVal(1)*T)*EVec(:,1)+...
        pp2*exp(EVal(2)*T)*EVec(:,2);           % p at t=0
    
heateq_par = struct('M',M,...
                'EVec',EVec,...
                'EVal',EVal,...
                'yTarget',y_target,...
                'alpha',1,...
                'xi',xi,...
                'n',n...
                );

fh = struct('tspan', tspan,...
            'Y0', y0,...
            'F',     @(t,y,u)(F(t,y,u,heateq_par)),...
            'JacFY', @(t,y,u)(JacFY(t,y,u,heateq_par)),...
            'JacFU', @(t,y,u)(JacFU(t,y,u,heateq_par)),...
            'ObjF',  @(y)(ObjF(y,heateq_par)),...
            'JacObjFY', @(y)(JacObjFY(y,heateq_par)),...
            'Aut', aut,...
            'yTRef', y_ref,...
            'p0Ref', p_ref,...
            'URefFunc', @(t)(URef(t,heateq_par))...
            );
        
end
            
function dydt = F(~,y,u,heateq_par)
M = heateq_par.M;
xi = heateq_par.xi;
n = heateq_par.n;
e_n = zeros(n,1); e_n(n) = 1;
gamma = 2/xi^2;
dydt = [M*y(1:n)+gamma*e_n*u; u^2];
end

function valJacFY = JacFY(~,~,~,heateq_par)
M = heateq_par.M;
n = heateq_par.n;
vec0r = zeros(1,n+1);
vec0c = zeros(n,1);
valJacFY = [M vec0c; vec0r];
end

function valJacFU = JacFU(~,~,u,heateq_par)
xi = heateq_par.xi;
n = heateq_par.n;
gamma = 2/xi^2;
vec0 = zeros(n-1,1);
valJacFU = [vec0; gamma; 2*u];
end

function valObjFunc = ObjF(yT,heateq_par)
alpha = heateq_par.alpha;
yTarget = heateq_par.yTarget;
n = heateq_par.n;
valObjFunc = 0.5*norm(yT(1:n)-yTarget,2)^2+0.5*alpha*yT(n+1);
end

function valJacObjFY = JacObjFY(yT,heateq_par)
alpha = heateq_par.alpha;
yTarget = heateq_par.yTarget;
n = heateq_par.n;
valJacObjFY = [yT(1:n)-yTarget; alpha/2];    
end

function valURef = URef(t,heateq_par)
xi = heateq_par.xi;
n = heateq_par.n;
EVal = heateq_par.EVal;
EVec = heateq_par.EVec;
T = 1;
gamma = 2/xi^2;
alpha = heateq_par.alpha;
pp1 = 1/75; 
pp2 = 1/75; 
pt1 = pp1*exp((T-t)*EVal(1));
pt2 = pp2*exp((T-t)*EVal(2));
valURef = -(pt1*EVec(n,1)+pt2*EVec(n,2))*gamma/alpha;
end

