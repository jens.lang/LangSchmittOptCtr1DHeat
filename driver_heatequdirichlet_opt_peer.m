function driver_heatequdirichlet_opt_peer
clc;

%% Input

%%% choose Peer triplet
%%% AP4o43bdf, AP4o43dif, AP4o43p, AP4o33pa, AP4o33pfs
meth = 'AP4o33pfs';
Coeffs = def_adjoint_peer_meth(meth);
name = Coeffs.name;
s = Coeffs.s;
c = Coeffs.c;

%%% define heat problem
ex = 'HeatEquDirichlet';
fh = feval([ex,'_init_opt']);

%%% number of time steps
N = [16 32 64 128 256 512];
tau = fh.tspan(2)./N;

%%% allocate auxiliary vectors
lN = length(N);
errU = zeros(lN,1);
errOptU = zeros(lN,1);
errY = zeros(lN,1);
errP = zeros(lN,1);
normGradCU = zeros(lN,1);
valC = zeros(lN,1);

%%% loop over runs
for i=1:lN
    
tPoints = zeros(s*N(i),1);
for j=1:N(i)
    for k=1:s
        tPoints((j-1)*s+k) = fh.tspan(1)+(c(k)+j-1)*tau(i);
    end
end

%%% reference values
refU = fh.URefFunc(tPoints);
refYT = fh.yTRef;
refP0 = fh.p0Ref;

%%% parameters for the Peer method
options_peer = struct('scheme',Coeffs,...
      'JacStrat','ConstJac',... AlwaysNew,ConstJac
      'LinearProblem',true,...
      'NewtonTol',1e-8,...
      'NewtonMax',100,...
      'NewtonFixed',false,...
      'MaxNSteps',1e+6,...
      'Autonomous',fh.Aut,...
      'InitialStep',tau(i)...
      ); 

%%% objective function and its gradient  
objfungrad = @(x)peer_opt(x,fh,options_peer); 
 
%%% constraints for optimal control
A = [];
b = [];
Aeq = [];
beq = [];
lb = [];
ub = [];
nonlcon = [];

%%% parameters for optimal control
options_opt = optimoptions('fmincon',...  
        'Algorithm','interior-point',... % interior-point
        'SpecifyObjectiveGradient',true,...
        'OptimalityTolerance',1e-14,...
        'ConstraintTolerance',1e-3,...
        'StepTolerance',1e-14,...
        'Display','FINAL',...            % iter, final, off
        'FiniteDifferenceType','central',...
        'CheckGradients',false);  

%%% initial values
U0 = 0*refU;
    
%%% call fmincon and store final control
[x,~,~,~] = ...
    fmincon(objfungrad,U0,A,b,Aeq,beq,lb,ub,nonlcon,options_opt);
errU(i) = norm(x-refU,inf);

%%% AP4o43p: eliminate controls U_k3 due to K_33=0
if strcmpi(meth,'AP4o43p')
    for k=1:N(i)-2
        x(3+k*s) = refU(3+k*s);
    end
errU(i) = norm(x-refU,inf);   
end

%%% AP4o33pfs: eliminate controls U_k1 due to K_11=0, K0_11=0
if strcmpi(meth,'AP4o33pfs')
    for k=1:N(i)
        x(1+(k-1)*s) = refU(1+(k-1)*s);
    end
errU(i) = norm(x-refU,inf);   
end

%%% compute approximations for yT and p0
[yT, p0, ~, pStageAll, C, gradCU] = peer_opt_conv(fh,options_peer,x);

%%% compute errors
errY(i) = norm(yT(1:end-1)-refYT,inf);
errP(i) = norm(p0(1:end-1)-refP0,inf);
normGradCU(i) = norm(gradCU,inf);
valC(i) = C;

%%% compute improved control vector
n = length(fh.Y0)-1;
alpha = 1;
optU = -2*n*n*pStageAll(n,:)'/alpha;
errOptU(i) = norm(refU-optU,inf);

end
%%% end loop over runs

%%% order of the method
fprintf('\n');
fprintf('Solver = %s\n',name);
fprintf('\n');
fprintf('Max-error of control U_ni\n');
for i=1:lN   
    fprintf(' %10.4e',errU(i));
end
fprintf('\n');
fprintf('Numerical order of the control approximation\n');
for i=2:lN
    errdiff=log(errU(i))-log(errU(i-1));
    hdiff=log(tau(i))-log(tau(i-1));
    fprintf(' %10.4e',errdiff/hdiff);
end
fprintf('\n');
%%% u = -gamma*e_n*p/alpha
fprintf('Max-error of control U_ni obtained by minimization of the Hamiltonian\n');
for i=1:lN   
    fprintf(' %10.4e',errOptU(i));
end
fprintf('\n');
fprintf('Numerical order of the improved control approximation\n');
for i=2:lN
    errdiff=log(errOptU(i))-log(errOptU(i-1));
    hdiff=log(tau(i))-log(tau(i-1));
    fprintf(' %10.4e',errdiff/hdiff);
end
fprintf('\n');
fprintf('Max-error of state\n');
for i=1:lN   
    fprintf(' %10.4e',errY(i));
end
fprintf('\n');
fprintf('Numerical order of the state approximation\n');
for i=2:lN
    errdiff=log(errY(i))-log(errY(i-1));
    hdiff=log(tau(i))-log(tau(i-1));
    fprintf(' %10.4e',errdiff/hdiff);
end
fprintf('\n');
fprintf('Max-error of costate\n');
for i=1:lN   
    fprintf(' %10.4e',errP(i));
end
fprintf('\n');
fprintf('Numerical order of costate approximation\n');
for i=2:lN
    errdiff=log(errP(i))-log(errP(i-1));
    hdiff=log(tau(i))-log(tau(i-1));
    fprintf(' %10.4e',errdiff/hdiff);
end
fprintf('\n');
fprintf('Residuum of gradient\n');
for i=1:lN   
    fprintf(' %10.4e',normGradCU(i));
end
fprintf('\n');
fprintf('Value of objective function\n');
for i=1:lN   
    fprintf(' %10.4e',valC(i));
end
fprintf('\n');


