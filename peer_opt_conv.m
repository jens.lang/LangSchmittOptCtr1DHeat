function [yT, p0, yStageAll, pStageAll, C, gradUC] = peer_opt_conv(fh,options,u)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  
%%%  PEER_OPT_CONV delivers 
%%%
%%%           yT - state solution at final time t=T,
%%%           p0 - adjoint solution at t=0,
%%%    yStageAll - state solution for all times,
%%%    pStageAll - adjoint solution for all times,
%%%            C - value of objective function,
%%%       gradUC - gradient of the objective function,
%%%
%%%  for a given control vector U.
%%%
%%%  U = (U_01,...,U_0s,U_11,...,U_1s,...,U_N1,...,U_Ns)^T
%%%  The control is a column vector of size (N+1)*s, where N+1 is the
%%%  number of times steps of the overall adjoint peer method including
%%%  initial and final step. It approximates the control variable u(t)
%%%  at time points t_m+c_ih, m=0,1,...,N, i=1,...,s.
%%%
%%%  yStageAll is a matrix of size (M,(N+1)*s), where M is the dimension
%%%  of the state variable and N+1 is the number of times steps of the 
%%%  overall adjoint peer method including initial and final step.
%%%
%%%  pStageAll is a matrix of size (M,(N+1)*s), where M is the dimension
%%%  of the adjoint variable and N+1 is the number of times steps of the 
%%%  overall adjoint peer method including initial and final step.
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funcF = fh.F;
funcJacY = fh.JacFY;
funcJacU = fh.JacFU;
funcObjF = fh.ObjF;
funcJacObjFY = fh.JacObjFY;
y0 = fh.Y0;
tspan = fh.tspan;
coeff = options.scheme;
h = options.InitialStep;
t_m = tspan(1);
t_end = tspan(end);
dimSys = length(y0);
s = coeff.s;
hFm = zeros(dimSys,s);
matPt = (coeff.A\coeff.B)';
matRt = (coeff.A\coeff.K)';
matS1t = coeff.S1';
matS2t = coeff.S2';
vecIP0 = coeff.vIP0;
vecIP1 = coeff.vIP1;
U0 = u(1:s);
u0 = U0'*vecIP0;
vecWN = coeff.vWN;
newJac = true;
noNewton = 0;
hmin = h + 10^-12;
U0 = u(1:s);
yStageAll = [];
JacS = spalloc(s*dimSys,s*dimSys,s*dimSys*dimSys);
Pm = zeros(dimSys,s);
Pm1 = zeros(dimSys,s);
vecPm = zeros(dimSys*s,1);
pStageAll = [];
INFONEWTON = 0;

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% start forward calculation
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%%% compute starting values
JacY = funcJacY(t_m,y0,u0);
if ~isempty(strfind(options.JacStrat,'ConstJac'))
   newJac = false;
end

[ Ym1, noIter ] = peerstart(fh,JacY,tspan,y0,u0,U0,options);
yStageAll = [yStageAll,Ym1];
noNewton = noNewton + noIter;
t_m = t_m + h;
m = 0;

%%% loop over time steps
while (t_m  < (t_end - hmin))
   
    m = m + 1;
    Ym = Ym1;
    Um = u((m-1)*s+1:m*s)';
    if newJac
        y1 = Ym1(:,1:s)*vecIP1;
        u1 = Um*vecIP1;
      JacY = funcJacY(t_m,y1,u1);
    end
    
    for i = 1:s
      
      hg = h*matRt(i,i);
      A = speye(dimSys) - hg*sparse(JacY);
      [ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(A);
      t_mi = t_m + coeff.c(i)*h;
      u_mi = u(m*s+i);
        gm =                   Ym * matPt(:,i)  ... 
                 +   hFm(:,1:i-1) * matRt(1:i-1,i);  
   
     % +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
     % ------ call Newton's method ------
        
     %%% initial values taken from polynomial fit at
     %%% t_m-1+hc_i,...,t_m-1+hc_s,t_m+hc_1,...,t_m+hc_i-1
     yk = Ym * matS1t(:,i) + Ym1 * matS2t(:,i);
     if options.LinearProblem
        yk = yk*0;
        options.NewtonTol = 1e20;
     end
        
     k = 0;        
     run_while = true;
        
     while run_while
            
       k = k+1;           
       hgf_yk = hg*funcF(t_mi,yk,u_mi);           
       g_yk = yk - gm - hgf_yk;           
       delta_yk = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ -g_yk))));           
       yk = yk + delta_yk;
            
       if ~options.NewtonFixed
          test = norm((delta_yk)./(1+abs(Ym(:,i))),'inf');
          run_while = k < 1 || (test > options.NewtonTol && ...
                      k < options.NewtonMax);
          if (k == options.NewtonMax && test > options.NewtonTol) 
             fprintf('Newton failed at t=%12.4e: iNo=%2d test=%12.4e\n',...
                  t_mi, k, test);
          end
       else
          run_while = k < options.NewtonMax;
       end
            
     end %%% end Newton's method
        
     noNewton = noNewton + k;
     Ym1(:,i) = yk;
     hFm(:,i) = h*funcF(t_mi,yk,u_mi);  
             
   end %%% end loop over stages

   yStageAll = [yStageAll,Ym1];
   t_m = t_m + h;
    
end %%% end loop over time steps

%%% compute final values
UN = u(end-s+1:end);
[ YmN, noIter ] = peerfinal(fh,JacY,Ym1,UN,options);
yStageAll = [yStageAll,YmN];
noNewton = noNewton + noIter;
yT = YmN(:,1:s)*vecWN;
m = m + 1;

%%% compute objective function
C = funcObjF(yT);

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% end forward calculation
%   solution vector: y(T), dim = dimSys
%   overall (stage) solution vector: yStageAll, dim = dimSys*(s*(N+1)) 
% storage: Yni = yStageAll(:,m*s+i), m=0,...,N, i=1,...,s
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% start backward calculation
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%%% adjoint initial values
N = m;
t_m = tspan(2)-h;
pT = funcJacObjFY(yT);

for i=1:s
       tNi = t_m + coeff.c(i)*h;
       yNi = yStageAll(:,N*s+i);
       uNi = u(end-s+i);
    JacNiT = fh.JacFY(tNi,yNi,uNi)';
   vecDiag = (i-1)*dimSys+1:i*dimSys;
      JacS(vecDiag,vecDiag) = JacNiT;
end

matANt = coeff.AN';
matKNt = coeff.KN';
A = kron(matANt,speye(dimSys)) - JacS*kron(h*matKNt,speye(dimSys));
[ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(A);
b = kron(vecWN,pT);
PN = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ b))));  
for i=1:s
    Pm(:,i) = PN((i-1)*dimSys+1:i*dimSys);
end
t_m = t_m - h;
pStageAll = [Pm,pStageAll];
noNewton = noNewton + 1;

% %%loop over time steps
matA = coeff.A;
matK = coeff.K;
hmin = 10^-12;
while (t_m > tspan(1) + hmin)
    
     if m==N
         matB = coeff.BN;
     else
         matB = coeff.B;
     end
     
     for i=s:-1:1
         t_mi = t_m + coeff.c(i)*h;
         u_mi = u((m-1)*s+i);        
         y_mi = yStageAll(:,(m-1)*s+i);
         JacT = fh.JacFY(t_mi,y_mi,u_mi)';
           hg = h*matK(i,i);
            A = matA(i,i)*speye(dimSys) - hg*sparse(JacT);
         [ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(A);
            b = Pm(:,1:s) * matB(1:s,i) - Pm1(:,i+1:s) * matA(i+1:s,i);
         Pm1(:,i) = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ b))));  
     end
     
      Pm = Pm1;
     t_m = t_m - h;
       m = m - 1;
     pStageAll = [Pm,pStageAll];
     noNewton = noNewton + s;
    
end %%% end loop over time steps

%%% adjoint final values
for i=1:s
       t0i = tspan(1) + coeff.c(i)*h;
       y0i = yStageAll(:,i);
       u0i = u(i);
    Jac0iT = fh.JacFY(t0i,y0i,u0i)';
   vecDiag = (i-1)*dimSys+1:i*dimSys;
      JacS(vecDiag,vecDiag) = Jac0iT;
end
matA0t = coeff.A0';
matK0t = coeff.K0';
 matBt = coeff.B';
A = kron(matA0t,speye(dimSys)) - JacS*kron(h*matK0t,speye(dimSys));
[ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(A);
for i=1:s
    vecPm(dimSys*(i-1)+1:dimSys*i) = Pm(:,i);
end
b = kron(matBt,speye(dimSys))*vecPm;
P0 = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ b))));  
for i=1:s
    Pm1(:,i) = P0((i-1)*dimSys+1:i*dimSys);
end
p0 = Pm1(:,1:s)*vecIP0;
pStageAll = [Pm1,pStageAll];
noNewton = noNewton + 1;

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% end backward calculation
%   solution vector: p(0), dim = dimSys
%   overall adjoint solution vector: pStageAll, dim = dimSys*(s*(N+1)) 
% storage: Pni = pStageAll(:,n*s+i), n=0,...,N, i=1,...,s
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if INFONEWTON == 1
   fprintf('Number of Newton Steps = %d for %d time steps.\n',...
       noNewton,2*(N+1));
end

%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% compute gradient
%   grad_U C(y_h(T))
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

pAllT = pStageAll';
gradUC = zeros(length(u)+1,1);

%%% u0
gradUF = funcJacU(tspan(1),y0,u0); 
vecB = coeff.vB;
for j=1:s
    gradUC(1) = gradUC(1) - h*vecB(j)*pAllT(j,:)*gradUF;
end

%%% u_0i
for i=1:s
    t_mi = tspan(1) + coeff.c(i)*h;
    gradUF = funcJacU(t_mi,yStageAll(:,i),u(i));
    vecK = coeff.K0(:,i);
    for j=1:s
        gradUC(1+i) = gradUC(1+i) - h*vecK(j)*pAllT(j,:)*gradUF;
    end
end
%%% u_1i,...,u_N-1,i
for k=1:N-1
    for i=1:s
        t_mi = tspan(1) + (coeff.c(i)+k)*h;
        gradUF = funcJacU(t_mi,yStageAll(:,k*s+i),u(k*s+i));
        Ki = coeff.K(i,i);
        gradUC(1+k*s+i) = - h*Ki*pAllT(k*s+i,:)*gradUF;
    end
end
%%% u_Ni
for i=1:s
    t_mi = tspan(1) + (coeff.c(i)+N)*h;
    gradUF = funcJacU(t_mi,yStageAll(:,N*s+i),u(N*s+i));
    vecK = coeff.KN(:,i);
    for j=1:s
        gradUC(1+N*s+i) = gradUC(1+N*s+i) - h*vecK(j)*pAllT(N*s+j,:)*gradUF;
    end
end

end %%% end function

% ----------------------------------------
% *** computing s starting values ***
% ----------------------------------------
function [Ym0,noIter] = peerstart(fh,JacY,tspan,y0,u0,U0,options)
  
coeff = options.scheme;
s = coeff.s;    
c = coeff.c;
vecA = coeff.vA;
vecB = coeff.vB;
matA0 = coeff.A0;
matK0 = coeff.K0;

funcF = fh.F;
h0 = options.InitialStep;
t0 = tspan(1);
h0F = h0*funcF(t0,y0,u0);

m = length(y0);
Ym0 = zeros(m,s);
Fk = zeros(m*s,1);
eS = ones(s,1);
Im = speye(m);
vecYm0 = kron(eS,y0);
A0m = kron(matA0,Im);
hK0m = kron(h0*matK0,Im);
hK0Jacm = kron(h0*matK0,JacY);
ItMat = sparse(A0m) - sparse(hK0Jacm);
[ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(ItMat);

gm = kron(vecA,y0) + kron(vecB,h0F);

k = 0;  
yk = vecYm0;
if options.LinearProblem
   yk = yk*0;
   options.NewtonTol = 1e20;
end
run_while = true;

while run_while

    k = k+1;   
    for i=1:s
       t0_i = t0 + c(i)*h0;
       u0_i = U0(i);
       yk_i = yk(m*(i-1)+1:m*i);
       Fk(m*(i-1)+1:m*i) = funcF(t0_i,yk_i,u0_i);
    end
    hgf_yk = hK0m*Fk;           
    g_yk = A0m*yk - gm - hgf_yk;           
    delta_yk = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ -g_yk))));           
    yk = yk + delta_yk;

    if ~options.NewtonFixed
      test = norm((delta_yk)./(1+abs(vecYm0)),'inf');
      run_while = k < 1 || (test > options.NewtonTol && ...
                  k < options.NewtonMax);
      if (k == options.NewtonMax && test > options.NewtonTol) 
         fprintf('Newton failed at t=%12.4e: iNo=%2d test=%12.4e\n',...
                  t0_i, k, test);
      end
    else
      run_while = k < options.NewtonMax;
    end

end %%% end Newton's method

for i=1:s
    Ym0(:,i) = yk(m*(i-1)+1:m*i);
end
noIter = k;

end

% ---------------------------------------
% *** computing s final values ***
% ---------------------------------------
function [YmN,noIter] = peerfinal(fh,JacY,YN,UN,options)
  
coeff = options.scheme;
s = coeff.s;    
c = coeff.c;
matAN = coeff.AN;
matBN = coeff.BN;
matKN = coeff.KN;
vecIP1 = coeff.vIP1;
vecIP0 = coeff.vIP0;

funcF = fh.F;
funcJacFY = fh.JacFY;
hN = options.InitialStep;
tN = fh.tspan(2)-hN;
m = length(YN(:,1));
YmN = zeros(m,s);
Fk = zeros(m*s,1);
Im = speye(m);
vecYmN = zeros(m*s,1);
for i=1:s
    vecYmN(m*(i-1)+1:m*i) = YN(:,i);
end
if ~isempty(strfind(options.JacStrat,'AlwaysNew'))
     y = YN(:,1:s)*vecIP1;
     u = UN'*vecIP0;
  JacY = funcJacFY(tN,y,u);
end

ANm = kron(matAN,Im);
BNm = kron(matBN,Im);
hKNm = kron(hN*matKN,Im);
hKNJacm = kron(hN*matKN,JacY);
ItMat = sparse(ANm) - sparse(hKNJacm);
[ LU.L, LU.U, LU.P, LU.Q, LU.R ] = lu(ItMat);

gm = BNm*vecYmN;

k = 0;  
yk = vecYmN;
if options.LinearProblem
   yk = yk*0;
   options.NewtonTol = 1e20;
end
run_while = true;

while run_while

    k = k+1;   
    for i=1:s
       tN_i = tN + c(i)*hN;
       yk_i = yk(m*(i-1)+1:m*i);
       uN_i = UN(i);
       Fk(m*(i-1)+1:m*i) = funcF(tN_i,yk_i,uN_i);
    end
    hgf_yk = hKNm*Fk;           
    g_yk = ANm*yk - gm - hgf_yk;           
    delta_yk = LU.Q * (LU.U \ (LU.L \ (LU.P * (LU.R \ -g_yk))));           
    yk = yk + delta_yk;

    if ~options.NewtonFixed
      test = norm((delta_yk)./(1+abs(vecYmN)),'inf');
      run_while = k < 1 || (test > options.NewtonTol && ...
                  k < options.NewtonMax);
      if (k == options.NewtonMax && test > options.NewtonTol) 
         fprintf('Newton failed at t=%12.4e: iNo=%2d test=%12.4e\n',...
                  tN_i, k, test);
      end
    else
      run_while = k < options.NewtonMax;
    end

end %%% end Newton's method

for i=1:s
    YmN(:,i) = yk(m*(i-1)+1:m*i);
end
noIter = k;

end

